class CreateNotes < ActiveRecord::Migration[6.1]
  def change
    create_table :notes do |t|
      t.text :text
      t.datetime :notification_date
      t.references :user

      t.timestamps
    end
  end
end
