class NotesController < ApiController
	before_action :authorized
	before_action :find_note, only: [:show, :update, :destroy]


	def create
		@note = logged_in_user.notes.create(note_params)

		if (@note.invalid?)
			return render_validation_errors
		end

		PushNotificationsWorker.perform_at(@note.notification_date, @note.id)

		render json: {note: @note}
	end

	def index
		@notes = logged_in_user.notes.all

		render json: {notes: notes}
	end

	def show
		render json: {note: @note}		
	end

	def update
		@note.update(note_params)

		if (@note.invalid?)
			return render_validation_errors
		end

		render json: {note: @note}
	end

	def destroy
		@note.destroy

		render json: {deleted_note: @note}		
	end

	private

	def render_validation_errors
		render json: { errors: @note.errors }, status: :bad_request
	end

	def find_note
		@note = logged_in_user.notes.find_by_id(params[:id])

		render json: { error: "Note not found" }, status: :not_found unless @note
	end

	def note_params
		params.permit(:text, :notification_date)
	end

end
