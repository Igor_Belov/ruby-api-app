class Note < ApplicationRecord
	validates :text, :notification_date, presence: true
	validate :notification_date_cannot_be_in_the_past

	private

	def notification_date_cannot_be_in_the_past
		if notification_date.present? && notification_date < DateTime.now
	      errors.add(:notification_date, "can't be in the past")
	    end
	end
end
