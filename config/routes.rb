require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  mount Sidekiq::Web => "/sidekiq"

  resource :user, only: [:create]
  resources :notes
  post "/login", to: "users#login"
end
